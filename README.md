# BE code


Code pour le bureau d'étude

Lucas Thu Ping One, Quentin Ducoulombier, Achille Furger, Faucher Noé


Pour compiler le programme :
  ``` 
  > make 
  ```

Pour lancer le programme :
  ``` 
  > ./exe
  ```
Puis suivre les indications dans le menu.

<br>

Pour modifier certains paramètres du programme, il faut les modifier directement dans les fichiers :

* config.h : pour les paramètres liés à l'équation à résoudre
* config_affichage.h : pour les paramètres liés à l'affichage des résultats
